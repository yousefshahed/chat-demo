import { useEffect, useRef, useState } from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { formatRelative } from "date-fns";
import { labels } from "./AFINN-ar";
const Sentiment = require("sentiment");
const sentiment = new Sentiment();

var arLanguage = {
  labels,
};

sentiment.registerLanguage("ar", arLanguage);

export default function ChatRoom(props) {
  const db = props.db;
  const SAD_EMOJI = [55357, 56864];
  const HAPPY_EMOJI = [55357, 56832];
  const NEUTRAL_EMOJI = [55357, 56848];
  const { uid, displayName, photoURL } = props.user;

  const dummySpace = useRef();

  const [newMessage, setNewMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const score = sentiment.analyze(newMessage).score;
    const arabicScore = sentiment.analyze(newMessage, { language: "ar" }).score;
    db.collection("messages").add({
      text: newMessage,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      sentiment: score || arabicScore,
      uid,
      displayName,
      photoURL,
    });

    setNewMessage("");

    // scroll down the chat
    dummySpace.current.scrollIntoView({ behavior: "smooth" });
  };
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    db.collection("messages")
      .orderBy("createdAt")
      .limit(100)
      .onSnapshot((querySnapShot) => {
        const data = querySnapShot.docs.map((doc) => ({
          ...doc.data(),
          id: doc.id,
        }));

        setMessages(data);
      });
  }, [db]);

  return (
    <main id="chat_room">
      <ul>
        {messages.map((message) => (
          <li key={message.id} className={message.uid === uid ? "sent" : "received"}>
            <section>
              {/* display user image */}
              {message.photoURL ? <img src={message.photoURL} alt="Avatar" width={45} height={45} /> : null}
            </section>

            <section>
              {/* display message text */}
              <p>
                {String.fromCodePoint(
                  ...(message.sentiment > 0 ? HAPPY_EMOJI : message.sentiment === 0 ? NEUTRAL_EMOJI : SAD_EMOJI)
                )}{" "}
                {message.text}
              </p>

              {/* display user name */}
              {message.displayName ? <span>{message.displayName}</span> : null}
              <br />
              {/* display message date and time */}
              {message.createdAt?.seconds ? (
                <span>{formatRelative(new Date(message.createdAt.seconds * 1000), new Date())}</span>
              ) : null}
            </section>
          </li>
        ))}
      </ul>
      <section ref={dummySpace}></section>

      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={newMessage}
          onChange={(e) => setNewMessage(e.target.value)}
          placeholder="Type your message here..."
        />

        <button type="submit" disabled={!newMessage}>
          Send
        </button>
      </form>
    </main>
  );
}
